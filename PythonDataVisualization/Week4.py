"""
Project for Week 4 of "Python Data Visualization".
Unify data via common country codes.

Be sure to read the project description page for further information
about the expected behavior of the program.
"""

import csv
import math
import pygal



def read_csv_as_nested_dict(filename, keyfield, separator, quote):
    """
    Inputs:
      filename  - Name of CSV file
      keyfield  - Field to use as key for rows
      separator - Character that separates fields
      quote     - Character used to optionally quote fields

    Output:
      Returns a dictionary of dictionaries where the outer dictionary
      maps the value in the key_field to the corresponding row in the
      CSV file.  The inner dictionaries map the field names to the
      field values for that row.
    """

    table = {}
    with open(filename,newline='') as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=separator, quotechar=quote)
        for row in csvreader:
            rowid = row[keyfield]
            table[rowid] = row
    return table

def build_country_code_converter(codeinfo):
    """
    Inputs:
      codeinfo      - A country code information dictionary

    Output:
      A dictionary whose keys are plot country codes and values
      are world bank country codes, where the code fields in the
      code file are specified in codeinfo.
    """

    table = {}


    with open(codeinfo['codefile'],newline='') as csvfile:
        csvreader = csv.DictReader(csvfile,
                                   delimiter=codeinfo['separator'],
                                   quotechar=codeinfo['quote']
                                   )
        for row in csvreader:
            table.update({row[codeinfo['plot_codes']]:row[codeinfo['data_codes']]})

    return table

def reconcile_countries_by_code(codeinfo, plot_countries, gdp_countries):
    """
    Inputs:
      codeinfo       - A country code information dictionary
      plot_countries - Dictionary whose keys are plot library country codes
                       and values are the corresponding country name
      gdp_countries  - Dictionary whose keys are country codes used in GDP data

    Output:
      A tuple containing a dictionary and a set.  The dictionary maps
      country codes from plot_countries to country codes from
      gdp_countries.  The set contains the country codes from
      plot_countries that did not have a country with a corresponding
      code in gdp_countries.

      Note that all codes should be compared in a case-insensitive
      way.  However, the returned dictionary and set should include
      the codes with the exact same case as they have in
      plot_countries and gdp_countries.
    """

    code_mapper_dict = build_country_code_converter(codeinfo)

    no_match_list = []
    mapped_names = {}

    # dictionary of country code: country name
    for plot_country in plot_countries.keys():

        matched = False

        # map the value from the plot library to the code mapper library ignoring case
        for code_mapper_key in code_mapper_dict:


            if str.upper(plot_country) == str.upper(code_mapper_key):

                # get the 3 digit code from the mapper file and then look for it in the GDP data
                mapped_gdp_key = code_mapper_dict[code_mapper_key]

                for country_code in gdp_countries:
                    if str.upper(mapped_gdp_key) == str.upper(country_code):
                        mapped_names.update({plot_country: country_code})
                        matched = True


        if not matched:
            no_match_list.append(plot_country)

    return mapped_names, set(no_match_list)


def build_map_dict_by_code(gdpinfo, codeinfo, plot_countries, year):
    """
    Inputs:
      gdpinfo        - A GDP information dictionary
      codeinfo       - A country code information dictionary
      plot_countries - Dictionary mapping plot library country codes to country names
      year           - String year for which to create GDP mapping

    Output:
      A tuple containing a dictionary and two sets.  The dictionary
      maps country codes from plot_countries to the log (base 10) of
      the GDP value for that country in the specified year.  The first
      set contains the country codes from plot_countries that were not
      found in the GDP data file.  The second set contains the country
      codes from plot_countries that were found in the GDP data file, but
      have no GDP data for the specified year.
    """

    no_gdp_value = []
    gdp_values = {}

    gdp_countries = read_csv_as_nested_dict(gdpinfo["gdpfile"],
                                            gdpinfo["country_code"],
                                            gdpinfo["separator"],
                                            gdpinfo["quote"])


    pygal_mapped_names, missing_countries_set = \
        reconcile_countries_by_code(codeinfo, plot_countries, gdp_countries)

    for key, val in pygal_mapped_names.items():
        gdp_string_value = gdp_countries[val][year]

        if gdp_string_value != '':
            gdp_log = math.log10(float(gdp_countries[val][year]))
            gdp_values.update({key:gdp_log})
        else:
            no_gdp_value.append(key)



    return gdp_values, missing_countries_set, set(no_gdp_value)


def render_world_map(gdpinfo, codeinfo, plot_countries, year, map_file):
    """
    Inputs:
      gdpinfo        - A GDP information dictionary
      codeinfo       - A country code information dictionary
      plot_countries - Dictionary mapping plot library country codes to country names
      year           - String year of data
      map_file       - String that is the output map file name

    Output:
      Returns None.

    Action:
      Creates a world map plot of the GDP data in gdp_mapping and outputs
      it to a file named by svg_filename.
    """
    gdp_values, no_match_name, no_match_data = \
        build_map_dict_by_code(gdpinfo, codeinfo, plot_countries, year)

    print(gdp_values)

    worldmap_chart = pygal.maps.world.World()
    worldmap_chart.title = 'GDP by Country'
    worldmap_chart.add('GDP Countries', gdp_values)
    worldmap_chart.add('No Match', no_match_name)
    worldmap_chart.add('No GDP Data', no_match_data)
    worldmap_chart.render_in_browser()

    return


def test_render_world_map():
    """
    Test the project code for several years
    """
    gdpinfo = {
        "gdpfile": "isp_gdp.csv",
        "separator": ",",
        "quote": '"',
        "min_year": 1960,
        "max_year": 2015,
        "country_name": "Country Name",
        "country_code": "Country Code"
    }

    codeinfo = {
        "codefile": "isp_country_codes.csv",
        "separator": ",",
        "quote": '"',
        "plot_codes": "ISO3166-1-Alpha-2",
        "data_codes": "ISO3166-1-Alpha-3"
    }

    # Get pygal country code map
    pygal_countries = pygal.maps.world.COUNTRIES




    render_world_map(
        gdpinfo,codeinfo,pygal_countries,'1985',"isp_gdp_test.svg")


    # 1960
    #render_world_map(
    # gdpinfo, codeinfo, pygal_countries, "1960", "isp_gdp_world_code_1960.svg")

    # 1980
    #render_world_map(
    # gdpinfo, codeinfo, pygal_countries, "1980", "isp_gdp_world_code_1980.svg")

    # 2000
    #render_world_map(
    # gdpinfo, codeinfo, pygal_countries, "2000", "isp_gdp_world_code_2000.svg")

    # 2010
    #render_world_map(
    # gdpinfo, codeinfo, pygal_countries, "2010", "isp_gdp_world_code_2010.svg")


# Make sure the following call to test_render_world_map is commented
# out when submitting to OwlTest/CourseraTest.

#test_render_world_map()