from queue import Queue

import copy


def goal_state(state):
    #actions = "UDLR"
    #postions = 012345678
    """
    find position of 0
    0 = 3
    :param state:
    :return:
    """
    if state == [0, 1, 2, 3, 4, 5, 6, 7, 8]:
        print(state)
        return True

def bfs_search (board, frontier, explored):
    """
    possible action depend on where the current empty space is; expanding the node means exploring all possible actions
    up, down, left, right
    up is possible if not in 0 row
    down is possible if not in 2 row
    left is possible if not in 0 column
    right is possible if not in 2 column

    find location of current open spot
    use functions to define moves (move_up, move down, move_left, move_right), returns a state, test the state

    :param init_state:
    :return:

    row, col = init_state.find

    frontier = Queue(init_state)
    explored = set()

    while not frontier.empty():
        state = frontier.get()
        explored.add(state)

        if goal_test(state):
            return True

        for neighbor in state.neighbors():
            if neighbor not in frontier and neighbor not in explored:
                frontier.put(neighbor)

    """

    explored = list()


    #initialize the frontier with the initial game board
    initial_state = list(board)
    initial_frontier = list(frontier)
    initial_frontier.append(initial_state)


    # evaluate the next node and if not the solution, do the search to expand nodes and add them to the frontier
    while (len(initial_frontier) > 0):
        eval_board = list(initial_frontier.pop())
        print("evaluating:",eval_board)

        if not goal_state(eval_board):
            #add to explorer and don't repeat
            explored.append(eval_board)


            print(eval_board,"is not the solution, expanding node")

            expanded_list = expand(eval_board, explored, initial_frontier)

            print("adding items: ", len(expanded_list), expanded_list)
            print("explored items:", len(explored))


            if (len(expanded_list) > 0):
                initial_frontier.extend(expanded_list)
                print("number of items:", len(initial_frontier) )
        else:
            print("solution found")


def expand(board, explored, frontier): #how to add the items to the list
    open_tile = board.index(0)
    expansion = list()


    if (open_tile > 2):
        up_board = move_up(board)
        if (up_board not in explored and up_board not in frontier):
            expansion.append(up_board)

    if (open_tile < 6):
        down_board = move_down(board)
        if (down_board not in explored and down_board not in frontier):
            expansion.append(down_board)

    if (open_tile not in (0,3,6)):
        left_board = move_left(board)
        if (left_board not in explored and left_board not in frontier):
            expansion.append(left_board)

    if (open_tile not in (2,5,8)):
        right_board = move_right(board)
        if (right_board not in explored and right_board not in frontier):
            expansion.append(right_board)



    return(expansion)




def find_open_tile(board):

    game_board = list(board)
    return game_board.index(0)



def move_up(board):
    new_board = list(board)
    position = new_board.index(0)

    val = new_board[position - 3]
    new_board[position - 3] = 0
    new_board[position] = val

    return new_board

def move_down(board):
    new_board = list(board)
    position = new_board.index(0)

    val = new_board[position + 3]
    new_board[position + 3] = 0
    new_board[position] = val

    return new_board


def move_left(board):
    new_board = list(board)
    position = new_board.index(0)

    val = new_board[position - 1]
    new_board[position - 1] = 0
    new_board[position] = val

    return new_board


def move_right(board):
    new_board = list(board)
    position = new_board.index(0)

    val = new_board[position + 1]
    new_board[position + 1] = 0
    new_board[position] = val

    return new_board






start_board = [1,2,5,3,4,0,6,7,8]
explored = set()
frontier = list()

bfs_search(start_board,frontier,explored)


