"""
Modify the following google_sector_report so that it returns a json
dump that contains the following information about each sector:
1. The sector name
2. The percentage change in sector value
3. The biggest gainer and the percentage change in the biggest gainer
4. The biggest loser and the percentage change in the biggest loser

The structure of the json is given in the assignment description on EdX.

Note:
To read files, use:

with open('filename') as f:
    lines = f.readlines()
"""

from bs4 import BeautifulSoup
import json

def google_sector_report():

    json_dict = {"result":
                     {"Energy":
                          {"biggest_gainer":
                               {"equity":"",
                                "change":0},
                           "biggest_loser":
                               {"equity": "",
                                "change": 0},
                           "change":0
                           },
                      "Basic Materials":
                          {"biggest_gainer":
                               {"equity": "",
                                "change": 0},
                           "biggest_loser":
                               {"equity": "",
                                "change": 0},
                           "change": 0
                           },
                      "Industrials":
                          {"biggest_gainer":
                               {"equity": "",
                                "change": 0},
                           "biggest_loser":
                               {"equity": "",
                                "change": 0},
                           "change": 0
                           }
                      }
                 }

    with open('Google Finance.htm') as file:
        html = BeautifulSoup(file.read(), 'lxml')

        energy_sector_tag = html.find('td', string='Energy')
        energy_sector_tag_change = float(energy_sector_tag.find_next_sibling('td').get_text().strip()[:-1])
        json_dict["result"]["Energy"]["change"] = energy_sector_tag_change

        with open('Energy.htm') as energy_file:
            energy_html = BeautifulSoup(energy_file.read(), 'lxml')

        movers_list = parse_mover(energy_html,'topmovers')
        json_dict["result"]["Energy"]["biggest_gainer"]["equity"] = movers_list[0][1]
        json_dict["result"]["Energy"]["biggest_gainer"]["change"] = movers_list[0][0]
        json_dict["result"]["Energy"]["biggest_loser"]["equity"] = movers_list[1][1]
        json_dict["result"]["Energy"]["biggest_loser"]["change"] = movers_list[1][0]


        #Basic Materials
        materials_sector_tag = html.find('td', string='Basic Materials')
        materials_sector_tag_change = float(materials_sector_tag.find_next_sibling('td').get_text().strip()[:-1])
        json_dict["result"]["Basic Materials"]["change"] = materials_sector_tag_change

        with open('Basic Materials.htm') as energy_file:
            energy_html = BeautifulSoup(energy_file.read(), 'lxml')

        movers_list = parse_mover(energy_html,'topmovers')
        json_dict["result"]["Basic Materials"]["biggest_gainer"]["equity"] = movers_list[0][1]
        json_dict["result"]["Basic Materials"]["biggest_gainer"]["change"] = movers_list[0][0]
        json_dict["result"]["Basic Materials"]["biggest_loser"]["equity"] = movers_list[1][1]
        json_dict["result"]["Basic Materials"]["biggest_loser"]["change"] = movers_list[1][0]

        #Industrials
        industrials_sector_tag = html.find('td', string='Industrials')
        industrials_sector_tag_change = float(industrials_sector_tag.find_next_sibling('td').get_text().strip()[:-1])
        json_dict["result"]["Industrials"]["change"] = industrials_sector_tag_change

        with open('Industrials.htm') as energy_file:
            energy_html = BeautifulSoup(energy_file.read(), 'lxml')

        movers_list = parse_mover(energy_html, 'topmovers')
        json_dict["result"]["Industrials"]["biggest_gainer"]["equity"] = movers_list[0][1]
        json_dict["result"]["Industrials"]["biggest_gainer"]["change"] = movers_list[0][0]
        json_dict["result"]["Industrials"]["biggest_loser"]["equity"] = movers_list[1][1]
        json_dict["result"]["Industrials"]["biggest_loser"]["change"] = movers_list[1][0]


    return json.dumps(json_dict)


def parse_mover(table, class_tag):
    """

    :param table: input html table
    :return: list of tuples with change % and equity name
    """
    gainers_list = []
    losers_list = []

    movers = table.find('table', class_=class_tag).find_all('tr')

    #gainers
    for x in range(1, 2):
        cols = movers[x].find_all('td')
        gainer = cols[0].get_text().strip()
        gainer_change = float(cols[3].get_text()[8:-3].strip('('))
        print(gainer_change)
        gainers_list.append((gainer_change, gainer))

    #losers
    for x in range(7,8):
        cols = movers[x].find_all('td')
        loser = cols[0].get_text().strip()
        loser_change = float(cols[3].get_text()[8:-3].strip('('))
        print(loser_change)
        losers_list.append((loser_change, loser))

    return [(gainers_list[0][0],gainers_list[0][1]),(losers_list[0][0],losers_list[0][1])]



print(google_sector_report())