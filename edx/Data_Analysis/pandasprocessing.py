import pandas as pd
import numpy as np
import datetime

"""
Put your script to transform the data in the transform() function.

You should read the 311_data.csv file in the work folder,
and use pandas to do the transformations.

Save the results as output1.csv.
"""

def transform():
    df = pd.read_csv("311_data.csv")

    df = df[['Created Date', 'Closed Date', 'Agency', 'Borough']]
    df = df[(df['Created Date'].notnull()) & (df['Closed Date'].notnull())]

    df['Created Date'] = df['Created Date'].apply(lambda x: datetime.datetime.strptime(x, '%m/%d/%Y %I:%M:%S %p'))
    df['Closed Date'] = df['Closed Date'].apply(lambda x: datetime.datetime.strptime(x, '%m/%d/%Y %I:%M:%S %p'))
    df['processing_time'] = df['Closed Date'] - df['Created Date']

    print(df)


transform()
