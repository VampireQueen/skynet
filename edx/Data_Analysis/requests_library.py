import requests
import json

"""
r = requests.get("https://en.wikipedia.org/wiki/main_page")
print(r.status_code)
result = r.content.decode("utf-8")
pos = result.find("Did you know")
print(pos)
"""

# Epicurious Example

def get_recipe(keywords):

    import requests
    from bs4 import BeautifulSoup

    recipe_list = []
    url = "http://www.epicurious.com/search/" + keywords

    response = requests.get(url)

    if not response.status_code == 200:
        print(response.status_code)
        return recipe_list

    results_page = BeautifulSoup(response.content, 'lxml')
    recipes = results_page.find_all('article', {'class': 'recipe-content-card'})

    for recipe in recipes:
        recipe_name = recipe.find('a').get_text()
        recipe_link = 'http://epicurious.com' + recipe.find('a').get('href')
        try:
            recipe_description = recipe.find('p', {'class': 'dek'}).get_text()
        except:
            recipe_description = ''

        recipe_list.append((recipe_name,recipe_link,recipe_description))

    return recipe_list

def get_recipe_info(recipe_link):
    import requests
    from bs4 import BeautifulSoup

    recipe_dict = {}

    response = requests.get(recipe_link)
    if not response.status_code == 200:
        return recipe_dict

    results_page = BeautifulSoup(response.content,'lxml')

    ingredients_list = []
    prep_steps_list = []

    for ingredient in results_page.find_all('li', {'class': 'ingredient'}):
        ingredients_list.append(ingredient.get_text())

    for prep_step in results_page.find_all('li', {'class': 'preparation-step'}):
        prep_steps_list.append(prep_step.get_text().strip())

    recipe_dict['ingredients'] = ingredients_list
    recipe_dict['preperation '] = prep_steps_list

    return recipe_dict

#print(get_recipe('tofu chili'))
print(get_recipe_info('http://epicurious.com/recipes/food/views/spicy-lemongrass-tofu-233844'))