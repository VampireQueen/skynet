import tensorflow as tf

placeholder_1 = tf.placeholder(dtype=tf.float32)
placeholder_2 = tf.placeholder(dtype=tf.float32)

multiply_node_1 = placeholder_1 * 3
multiply_node_2 = placeholder_1 * placeholder_2

#session = tf.Session()
#print(session.run(multiply_node_2, {placeholder_1: [4.0,5.0], placeholder_2: [2.0,5.0]}))

#Variable Nodes
var_node_1 = tf.Variable([5.0],dtype=tf.float32)
const_node_1 = tf.constant([10.0],dtype=tf.float32)

session = tf.Session()

session.run(tf.global_variables_initializer())

print(session.run(var_node_1 * const_node_1))

session.run(var_node_1.assign([10.0]))
print(session.run(var_node_1))
