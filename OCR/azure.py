"""
Import Requests Library


"""
import requests
import json

def extract(image):
    subscription_key = '75fb28abc9704a8881ab1b91a7d85644'

    ocr_url = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/ocr?language=unk"

    headers = {'Content-Type': 'application/octet-stream', 'Ocp-Apim-Subscription-Key': subscription_key }
    params = {'language': 'unk', 'detectOrientation ': 'true'}
    image_url = {'url': 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Atomist_quote_from_Democritus.png/338px-Atomist_quote_from_Democritus.png'}

    #from web with their sample image
    #response = requests.post(ocr_url, headers=headers, params=params, json=image_url)

    #from file
    response = requests.post(ocr_url, headers=headers, params=params, data=image)


    response.raise_for_status()
    analysis = response.json()

    print(json.dumps(analysis, sort_keys=True, indent=4))
    #data_dict = json.load(analysis)

    return analysis

def load_image(file_location):

    with open(file_location, "rb") as f:
        image_data = f.read()

    return(image_data)


print("loading image")
pic = load_image('sample.png')
print(type(pic))
extract(pic)
