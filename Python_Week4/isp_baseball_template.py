"""
Project for Week 4 of "Python Data Analysis".
Processing CSV files with baseball stastics.

Be sure to read the project description page for further information
about the expected behavior of the program.
"""

import csv

##
## Provided code from Week 3 Project
##

def read_csv_as_list_dict(filename, separator, quote):
    """
    Inputs:
      filename  - name of CSV file
      separator - character that separates fields
      quote     - character used to optionally quote fields
    Output:
      Returns a list of dictionaries where each item in the list
      corresponds to a row in the CSV file.  The dictionaries in the
      list map the field names to the field values for that row.
    """
    table = []
    with open(filename, newline='') as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=separator, quotechar=quote)
        for row in csvreader:
            table.append(row)
    return table


def read_csv_as_nested_dict(filename, keyfield, separator, quote):
    """
    Inputs:
      filename  - name of CSV file
      keyfield  - field to use as key for rows
      separator - character that separates fields
      quote     - character used to optionally quote fields
    Output:
      Returns a dictionary of dictionaries where the outer dictionary
      maps the value in the key_field to the corresponding row in the
      CSV file.  The inner dictionaries map the field names to the
      field values for that row.
    """
    table = {}
    with open(filename, newline='') as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=separator, quotechar=quote)
        for row in csvreader:
            rowid = row[keyfield]
            table[rowid] = row
    return table

##
## Provided formulas for common batting statistics
##

# Typical cutoff used for official statistics
MINIMUM_AB = 500

def batting_average(info, batting_stats):
    """
    Inputs:
      batting_stats - dictionary of batting statistics (values are strings)
    Output:
      Returns the batting average as a float
    """
    hits = float(batting_stats[info["hits"]])
    at_bats = float(batting_stats[info["atbats"]])
    if at_bats >= MINIMUM_AB:
        return hits / at_bats
    else:
        return 0

def onbase_percentage(info, batting_stats):
    """
    Inputs:
      batting_stats - dictionary of batting statistics (values are strings)
    Output:
      Returns the on-base percentage as a float
    """
    hits = float(batting_stats[info["hits"]])
    at_bats = float(batting_stats[info["atbats"]])
    walks = float(batting_stats[info["walks"]])
    if at_bats >= MINIMUM_AB:
        return (hits + walks) / (at_bats + walks)
    else:
        return 0

def slugging_percentage(info, batting_stats):
    """
    Inputs:
      batting_stats - dictionary of batting statistics (values are strings)
    Output:
      Returns the slugging percentage as a float
    """
    hits = float(batting_stats[info["hits"]])
    doubles = float(batting_stats[info["doubles"]])
    triples = float(batting_stats[info["triples"]])
    home_runs = float(batting_stats[info["homeruns"]])
    singles = hits - doubles - triples - home_runs
    at_bats = float(batting_stats[info["atbats"]])
    if at_bats >= MINIMUM_AB:
        return (singles + 2 * doubles + 3 * triples + 4 * home_runs) / at_bats
    else:
        return 0


##
## Part 1: Functions to compute top batting statistics by year
##

def filter_by_year(statistics, year, yearid):
    """
    Inputs:
      statistics - List of batting statistics dictionaries
      year       - Year to filter by
      yearid     - Year ID field in statistics
    Outputs:
      Returns a list of batting statistics dictionaries that
      are from the input year.
    """

    new_list = list(filter(lambda byyear: byyear[yearid] == str(year), statistics))
    return new_list


def top_player_ids(info, statistics, formula, numplayers):
    """
    Inputs:
      info       - Baseball data information dictionary
      statistics - List of batting statistics dictionaries
      formula    - function that takes an info dictionary and a
                   batting statistics dictionary as input and
                   computes a compound statistic
      numplayers - Number of top players to return
    Outputs:
      Returns a list of tuples, player ID and compound statistic
      computed by formula, of the top numplayers players sorted in
      decreasing order of the computed statistic.
    """

    new_list = list(map(lambda dict:
                        (dict[info['playerid']],
                        formula(info,dict)),
                        statistics)
                    )

    new_list.sort(key=lambda pair: pair[1],reverse=True)

    top_ten_list = new_list[0:numplayers]

    return top_ten_list


def lookup_player_names(info, top_ids_and_stats):
    """
    Inputs:
      info              - Baseball data information dictionary
      top_ids_and_stats - list of tuples containing player IDs and
                          computed statistics
    Outputs:
      List of strings of the form "x.xxx --- FirstName LastName",
      where "x.xxx" is a string conversion of the float stat in
      the input and "FirstName LastName" is the name of the player
      corresponding to the player ID in the input.
    """

    stats_dict = read_csv_as_nested_dict(info["masterfile"],info['playerid'],
                                       info["separator"],
                                       info["quote"])


    #convert to list of lists
    stats_list = list(map(lambda lst:
                          "{:1.3f}".format(lst[1]) + " --- " +
                          stats_dict[lst[0]][info['firstname']] + ' ' +
                          stats_dict[lst[0]][info['lastname']],
                          top_ids_and_stats))

    return stats_list



def compute_top_stats_year(info, formula, numplayers, year):
    """
    Inputs:
      info        - Baseball data information dictionary
      formula     - function that takes an info dictionary and a
                    batting statistics dictionary as input and
                    computes a compound statistic
      numplayers  - Number of top players to return
      year        - Year to filter by
    Outputs:
      Returns a list of strings for the top numplayers in the given year
      according to the given formula.
    """
    stats_list = read_csv_as_list_dict(info["battingfile"],
                                       info["separator"],
                                       info["quote"])

    new_list = list(filter(lambda byyear: byyear[info['yearid']] == str(year), stats_list))

    new_list_top_players = top_player_ids(info,new_list,formula,numplayers)

    new_list_top_players_name = lookup_player_names(info,new_list_top_players)

    return new_list_top_players_name


##
## Part 2: Functions to compute top batting statistics by career
##

def aggregate_by_player_id(statistics, playerid, fields):
    """
    Inputs:
      statistics - List of batting statistics dictionaries
      playerid   - Player ID field name
      fields     - List of fields to aggregate
    Output:
      Returns a nested dictionary whose keys are player IDs and whose values
      are dictionaries of aggregated stats.  Only the fields from the fields
      input will be aggregated in the aggregated stats dictionaries.
    """

    aggregate_stats_dict = {}

    new_field_list = list(fields)
    new_field_list.append(playerid)

    for stat_dictionary in statistics:
        player = stat_dictionary[playerid]

        if player in aggregate_stats_dict:
            for field in new_field_list:
                if field != playerid:
                    aggregate_stats_dict[player][field] = \
                        int(int(aggregate_stats_dict[player][field])
                          + int(stat_dictionary[field]))
        else:
            new_entry_stats = {} #inner dictionary


            stat_fields = [] #list of tuples for nested dict stats
            for field in new_field_list:
                if field != playerid:
                    stat_fields.append((field,int(stat_dictionary[field])))
                else:
                    stat_fields.append((field, stat_dictionary[field]))

            new_entry_stats.update(stat_fields)

            aggregate_stats_dict.update([(player,new_entry_stats)])


    return aggregate_stats_dict


def compute_top_stats_career(info, formula, numplayers):
    """
    Inputs:
      info        - Baseball data information dictionary
      formula     - function that takes an info dictionary and a
                    batting statistics dictionary as input and
                    computes a compound statistic
      numplayers  - Number of top players to return
    """

    stats_list = read_csv_as_list_dict(info["battingfile"],
                                       info["separator"],
                                       info["quote"])



    aggregated_stats_dict = aggregate_by_player_id(
        stats_list,info['playerid'],info['battingfields']
    )

    aggregated_stats_list = []
    aggregated_stats_list = list(aggregated_stats_dict.values())

    new_list_top_players = top_player_ids(info,aggregated_stats_list,formula,numplayers)

    new_list_top_players_name = lookup_player_names(info,new_list_top_players)
    #print(new_list_top_players_name)

    return new_list_top_players_name


