# Uses python3
def calc_fib(n):

    if n >= 2:

        F = list()
        F.append(0)
        F.append(1)

        for i in range(2, n + 1):  # range is up to but not including so add 1
            F.append(F[i - 1] + F[i - 2])

        return F[n]

    else:
        return n


#n = int(input())
print(calc_fib(200000))
