# Uses python3
import sys

def get_fibonacci_last_digit_naive(n):
    if n <= 1:
        return n

    previous = 0
    current  = 1

    for _ in range(n - 1):
        previous, current = current, previous + current

    return current % 10

def get_fibonacci_last_digit(n):

    if n >= 2:

        F = list()
        F.append(0)
        F.append(1)

        for i in range(2, n + 1):  # range is up to but not including so add 1
            F.append((F[i - 1] + F[i - 2]) % 10)

        return F[n]

    else:
        return n


    return


if __name__ == '__main__':
    input = sys.stdin.read()
    n = int(input)
    print(get_fibonacci_last_digit(n))

    #for n in range (327305,327306):
    #    print(get_fibonacci_last_digit_naive(n))
    #    print(get_fibonacci_last_digit(n))
    #    print('-----------')
