def Fib(n):
    n = n + 1

    F = list()
    F.append(0)
    F.append(1)

    for i in range(2,n+1):
        F.append(F[i-1] + F[i-2])

    return F[n]

#print(Fib(200))


#key lemma
def GCD(a, b):
    if b == 0:
        return a
    else:
        a_prime = a % b

        ans = GCD(b, a_prime)

    return ans

n = 1653264
m = 3918848

print(GCD(n,m))




