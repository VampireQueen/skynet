# Uses python3
import sys
import random

def get_change(m):
    # smallest number of coins to make the input value, coins can be 10, 5, and 1
    # fit as many 10s as you call, then take the remainder and fit as many 5s and then the remainder in 1s

    coins = 0
    remainder = m

    if remainder >= 10:
        coins = remainder // 10
        remainder = remainder % 10

    if remainder >= 5:
        coins = coins + remainder // 5
        remainder = remainder % 5

    coins = coins + remainder

    return coins

if __name__ == '__main__':
    m = int(sys.stdin.read())
    print(get_change(m))

    """
    for _ in range(100):
        m = random.randrange(0,1000)
        print(m, get_change(m))

    m = 0
    print(m, get_change(m))

    m = 1
    print(m, get_change(m))

    m = 5
    print(m, get_change(m))

    m = 10
    print(m, get_change(m))
    """