# Uses python3
import sys
import math

def lcm_naive(a, b):
    for l in range(1, a*b + 1):
        if l % a == 0 and l % b == 0:
            return l

    return a*b

def GCD(a,b):
    return math.gcd(a,b)


def lcm(a,b):
    """
    a*b / gcd =lcm
    :param a: first number
    :param b: second number
    :return: lowest common multiple of the two numbers (smallest number
    that can be divided by both a and b without a remainder, e.g. 24 for 6 and 8
    """

    greatest_common_divisor = GCD(a,b)
    return (a * b )// greatest_common_divisor




if __name__ == '__main__':
    """
    input = sys.stdin.read()
    a, b = map(int, input.split())
    print(lcm(a, b))

    """
    a = 21
    b = 34
    print(lcm(a,b))


