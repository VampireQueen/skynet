# Uses python3
import sys
from collections import namedtuple
from operator import itemgetter

Segment = namedtuple('Segment', 'start end')

def optimal_points(segments):
    points = []


    #write your code here

    #dont modify the arguments
    segs = list(segments)

    while len(segs) > 0:
        endpoint = segs[0].end
        #print(endpoint)

        points.append(endpoint)

        index = 0
        done = False

        while not done:
            if segs[index].start <= endpoint:
                segs.pop(index)

                if len(segs) == 0:
                    return points
            else:
                index = index + 1

            if index == len(segs):
                done = True


    return points

if __name__ == '__main__':
    input = sys.stdin.read()
    n, *data = map(int, input.split())

    segments = list(map(lambda x: Segment(x[0], x[1]), zip(data[::2], data[1::2])))
    #segments = [Segment(start=1, end=2), Segment(start=4, end=7), Segment(start=1, end=3), Segment(start=2, end=5), Segment(start=5, end=6)]
    segments = sorted(segments, key=itemgetter(1))

    #print(segments)

    points = optimal_points(segments)

    print(len(points))
    for p in points:
        print(p, end=' ')
