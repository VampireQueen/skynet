# Uses python3
import sys

def fibonacci_sum_naive(n):
    if n <= 1:
        return n

    previous = 0
    current  = 1
    sum      = 1

    for _ in range(n - 1):
        previous, current = current, previous + current
        sum += current

    return sum % 10


def fibonacci_sum(n):
    if n <= 1:
        return n

    """
    n = how many fibonacci numbers to create (fibonacci number is sum of prior two numbers)
    m = number to divide by and get the remainder

    need to avoid creating the fibonacci sequence for all 0 to n (this will take too long)
    if length of the pisano sequence is known, since the pattern just repeats, you can just use the mod to 
    get the index in the repeating pattern  you need

    e.g. n = 2015, pisano = 8, 2015 mod 8 = 7; the value at index 7 is what is needed. 
    """
    n = n + 2

    # Fibannaci modulo series for n
    F = list()
    F.append(0)
    F.append(1)

    if n > (60):
        max_range = n % 60
    else:
        max_range = n

    # create a list of the fib# mod m; this list will have some repeating pattern
    for i in range(2, max_range + 1):  # range is up to but not including so add 1
        F.append((F[i - 1] + F[i - 2]) % 10)


    # the remainder is the index of the item in the pisano to return
    fib_plus_2 = F[max_range]
    if fib_plus_2 == 0:
        return 9
    else:
        return fib_plus_2 - 1





if __name__ == '__main__':

    input = sys.stdin.read()
    n = int(input)
    print(fibonacci_sum(n))
    """
    #print(fibonacci_sum(832564823476))
    #print(fibonacci_sum(614162383528))
    """