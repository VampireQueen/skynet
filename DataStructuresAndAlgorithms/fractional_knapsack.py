# Uses python3
import sys

def get_optimal_value(capacity, weights, values):
    value = 0.

    # write your code here
    # create a list of tuples so we can sort by highest price per lb.


    number_of_items = len(weights)
    price_per_lb_list = []

    for num in range(0,number_of_items):
        val = values[num] / weights[num]
        price_per_lb_list.append((val,weights[num]))


    price_per_lb_list.sort(reverse=True)

    remaining_capacity = capacity

    for tup_item in price_per_lb_list:
        #subtract the next weight from the remaining capacity

        item_weight = tup_item[1]

        if item_weight > remaining_capacity:
            # add fractional amount
            value = value + (remaining_capacity / item_weight) * (tup_item[0] * tup_item[1])
        else:
            value = value + (tup_item[0] * tup_item[1])

        #print(remaining_capacity,item_weight,tup_item[0])

        # subtract from remaining capacity
        remaining_capacity = remaining_capacity - item_weight

        # if no more capacity return the current value; otherwise go to the next most valuable item
        if remaining_capacity <= 0:
            return value

    return value


if __name__ == "__main__":
    data = list(map(int, sys.stdin.read().split()))
    #data = [1, 10, 500, 30]

    n, capacity = data[0:2]
    values = data[2:(2 * n + 2):2]
    weights = data[3:(2 * n + 2):2]

    opt_value = get_optimal_value(capacity, weights, values)
    print("{:.10f}".format(opt_value))

"""
[3, 50, 60, 20, 100, 50, 120, 30]
[60, 100, 120]
[20, 50, 30]
0.0000000000"
"""