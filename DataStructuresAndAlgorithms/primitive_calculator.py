# Uses python3
import sys

def optimal_sequence(n):
    sequence = []
    while n >= 1:
        sequence.append(n)
        if n % 3 == 0:
            n = n // 3
        elif n % 2 == 0:
            n = n // 2
        else:
            n = n - 1
    return reversed(sequence)



d = {}

def f(n):
    print("calling f(n) for : ", n)

    if n == 1:
        print("returning answer 1,-1")
        return 1, -1

    #check the dictionary to see if this value is already calculated
    if d.get(n) is not None:
        print("returning cached answer ", d[n])
        return d[n]

    ans = (f(n - 1)[0] + 1, n - 1)

    print("first answer ", ans)


    if n % 2 == 0:
        print("calling f(n) for n//2 : ", n //2)
        ret = f(n // 2)
        if ans[0] > ret[0]:
            ans = (ret[0] + 1, n // 2)
            print("revised answer ", ans)

    if n % 3 == 0:
        print("calling f(n) for n//3 : ", n // 3)
        ret = f(n // 3)
        if ans[0] > ret[0]:
            ans = (ret[0] + 1, n // 3)
            print("revised answer ", ans)

    print("returning function where n=",n, "answer =", ans)
    d[n] = ans
    print("dictionary = ",d)
    print("-------")
    return ans

def print_solution(n):
    if f(n)[1] != -1:
        print_solution(f(n)[1])
    print (n),

def solve(n):
    result = f(n)
    print(result)

solve(5)



"""


input = sys.stdin.read()
n = int(input)
sequence = list(optimal_sequence(n))
print(len(sequence) - 1)
for x in sequence:
    print(x, end=' ')
"""
