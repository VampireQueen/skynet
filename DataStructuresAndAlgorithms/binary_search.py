# Uses python3
import sys
import math

def binary_search(a, left, right, x):

    # write your code here
    #print("binary search in ", left, right)

    if left > right:
        return -1

    mid = left + ((right - left) // 2)


    if x == a[mid]:
        return mid

    elif x < a[mid]:
        return binary_search(a,left,mid-1,x)
    else:
        return binary_search(a,mid+1,right,x)

    return 0

def linear_search(a, x):
    for i in range(len(a)):
        if a[i] == x:
            return i
    return -1

if __name__ == '__main__':
    input = sys.stdin.read()
    data = list(map(int, input.split()))

    #data = [5,1,5,8,12,13,5,8,1,23,1,11]

    n = data[0]
    m = data[n + 1]
    a = data[1 : n + 1]

    #print(n,m,a)

    left, right = 0, len(a) -1

    for x in data[n + 2:]:
        # replace with the call to binary_search when implemented
        #print(linear_search(a, x), end = ' ')
        #print("x = ",x)
        print(binary_search(a,left,right,x))
