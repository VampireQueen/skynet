# Uses python3
import sys

def optimal_weight(W, w):

    """

    :param W: maximum weight of the container
    :param w: contains number of items in index 0, and then weights of each item
    :return: return last index of last array?
    """

    """
    initialize a matrix (as list of lists) with a sublist for
    each item
    """

    item_weights_list = w
    num_items = len(w)
    matrix_max_index = W + 1

    opt_matrix = [[0] * matrix_max_index for index in range(num_items)]

    #update the first row of the matrix
    opt_matrix[0] = [item_weights_list[0] if item_weights_list[0] <= index else 0 for index in range(matrix_max_index)]
    #print(opt_matrix)
    #print("--------------------")
    #print("--------------------")

    for i in range(1, num_items):
        for j in range(1, matrix_max_index):

            #get initial value from prior row and then see if it can be improved
            prior_row = i-1

            value = opt_matrix[prior_row][j]
            #print("initial value for (i=", i, ",j=", j, ") is ",value, "from prior row")
            #print("item weight",item_weights_list[i])

            if item_weights_list[i] <= j:

                row = i - 1
                column = j - item_weights_list[i]

                potential_new_value = opt_matrix[row][column] + item_weights_list[i]

                #print("potential new value is looking at [",row,",",column,"] =",opt_matrix[row][column],"+ item weight",item_weights_list[i])
                #print("is potential new value",potential_new_value, ">", value,"?")

                if potential_new_value > value:
                    value = potential_new_value
                    opt_matrix[i][j] = value
                else:
                    opt_matrix[i][j] = value
            else:
                opt_matrix[i][j] = value  # set to [i - 1][j]
            #print(opt_matrix)
            #print("--------------------")

        #print(opt_matrix)
        #print("--------------------")
        #print("--------------------")

    return opt_matrix[-1][-1]

if __name__ == '__main__':
    input = sys.stdin.read()
    W, n, *w = list(map(int, input.split()))

    #W = 10
    #w = [1,4,8]
    #print(w)

    print(optimal_weight(W, w))
