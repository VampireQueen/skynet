# Uses python3
import sys

def optimal_summands(n):
    summands = []
    #write your code here

    if n == 1:
        summands.append(1)
        return summands

    if n == 2:
        summands.append(2)
        return summands

    prize = 1
    remainder = n - 1
    summands.append(prize)

    while remainder > 0:
        next_prize = remainder - (prize + 1)

        if next_prize > (prize + 1):
            summands.append(prize + 1)
            remainder = remainder - (prize + 1)
            prize = prize + 1
        else:
            summands.append(remainder)
            remainder = 0

    return summands

if __name__ == '__main__':
    input = sys.stdin.read()
    n = int(input)

    #n = 21
    summands = optimal_summands(n)



    print(len(summands))
    for x in summands:
        print(x, end=' ')
