# Uses python3
import sys

def get_fibonacci_huge_naive(n, m):
    if n <= 1:
        return n

    previous = 0
    current  = 1

    for _ in range(n - 1):
        previous, current = current, previous + current

    return current % m

def get_fibonacci_huge(n,m):
    if n <= 1:
        return n

    """
    n = how many fibonacci numbers to create (fibonacci number is sum of prior two numbers)
    m = number to divide by and get the remainder
    
    need to avoid creating the fibonacci sequence for all 0 to n (this will take too long)
    if length of the pisano sequence is known, since the pattern just repeats, you can just use the mod to 
    get the index in the repeating pattern  you need
    
    e.g. n = 2015, pisano = 8, 2015 mod 8 = 7; the value at index 7 is what is needed. 
    """

    # Fibannaci modulo series for n
    F = list()
    F.append(0)
    F.append(1)

    if n > (m * 6):
        max_range = m * 6 * 2
    else:
        max_range = n

    # create a list of the fib# mod m; this list will have some repeating pattern
    for i in range(2, max_range + 1):  # range is up to but not including so add 1
        F.append((F[i - 1] + F[i - 2]) % m)


    #find the pattern
    pisano = 1
    pisano = find_pattern(F,max_range)

    # when the length is known then divide n by the len(pisano) and then get the remainder
    pisano_index = n % pisano

    # the remainder is the index of the item in the pisano to return
    return F[pisano_index]

# is this too slow?
def find_pattern(numbers_list, max_range):

    # print("len F - ", len(numbers_list))
    # print("max range - ", max_range)
    #print(numbers_list)

    num_length = len(numbers_list)
    pattern_length = num_length


    # find the occurrances of 0
    zero_count = numbers_list.count(0)
    #print('number of occurences of element 0: ',zero_count)

    if zero_count == 1:
        return pattern_length

    next_zero_index = 2
    next_number = 0

    for x in range(zero_count-1):

        next_zero_index = numbers_list.index(0,next_zero_index + 1)
        #print("zero found at index: ",next_zero_index)

        #test if next number is 1
        next_number = numbers_list[next_zero_index+1]
        if next_number == 1:
            #print(next_zero_index)
            return next_zero_index



if __name__ == '__main__':
    """
    input = sys.stdin.read();
    n, m = map(int, input.split())
    print(get_fibonacci_huge(n, m))
    """
    
    #print(get_fibonacci_huge(2816213588,30524))
    #print(get_fibonacci_huge(239, 1000))
    #print(get_fibonacci_huge(1,239))
    #print(get_fibonacci_huge(2015, 3))
    
    #print(get_fibonacci_huge(99999999999999999,5))
    #print(get_fibonacci_huge(2816213588, 30524))
    #print(get_fibonacci_huge(99999999999999999,40000))


    print(get_fibonacci_huge(6 + 2, 10) - 1)
