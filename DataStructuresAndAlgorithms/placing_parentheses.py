# Uses python3
import math

digits = [0]
operators = []
min_matrix = {}
max_matrix = {}

def evalt(a, op, b):
    if op == '+':
        return a + b
    elif op == '-':
        return a - b
    elif op == '*':
        return a * b
    else:
        assert False

def MinAndMax(i, j):
    min_val = float('inf')
    max_val = -float('inf')

    for k in range (i, j):
        #print("--------------")
        #print("analyzig cell",i,j,"k=",k)

        #print("evaluating a")
        #print(max_matrix[i, k], operators[k-1], max_matrix[k + 1, j])
        a = evalt(max_matrix[i, k], operators[k-1], max_matrix[k + 1, j])

        #print("evaluating b")
        #print(max_matrix[i, k], operators[k-1], max_matrix[k + 1, j])
        b = evalt(max_matrix[i, k], operators[k-1], max_matrix[k + 1, j])

        #print("evaluating c")
        #print(min_matrix[i, k], operators[k-1], min_matrix[k + 1, j])
        c = evalt(min_matrix[i, k], operators[k-1], min_matrix[k + 1, j])

        #print("evaluating d")
        #print(min_matrix[i, k], operators[k-1], min_matrix[k + 1, j])
        d = evalt(min_matrix[i, k], operators[k-1], min_matrix[k + 1, j])

        min_val = min(min_val,a,b,c,d)
        max_val = max(max_val,a,b,c,d)


    #print("for values",i,j,"min value =", min_val, "max value =",max_val)

    return min_val, max_val


def get_maximum_value(dataset):

    #what is n?  number of digits in the expression
    num_operations = (len(dataset) // 2) + 1
    start_index = 1
    end_index = num_operations + 1

    #create list of integers and list of operations
    for n in range(0,len(dataset),2):
        digits.append(int(dataset[n:n+1]))

        if n + 1 != len(dataset):
            operators.append(dataset[n+1:n+2])

    #print(digits)
    #print(operators)

    #create a dictionary to store the matrix values


    for i in range(start_index,end_index): min_matrix[i,i]=digits[i]
    for i in range(start_index,end_index): max_matrix[i,i]=digits[i]



    for s in range(start_index,end_index-1):
        for i in range (start_index,end_index-s):
            j = i + s

            min_matrix[i,j], max_matrix[i,j] = MinAndMax(i,j)


    #print("min matrix:", min_matrix)
    #print("max matrix:", max_matrix)
    return max_matrix[start_index,end_index-1]


if __name__ == "__main__":
    print(get_maximum_value(input()))

    #print(get_maximum_value("5-8+7*4-8+9"))