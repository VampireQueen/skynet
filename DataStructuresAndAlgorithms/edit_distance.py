# Uses python3
def edit_distance(s, t):
    """

    :param s: string #1
    :param t: string #2
    :return: edit distance
    """

    #string 1 length is the number of columns in the matrix
    string1_len =len(s)+1

    #string 2 length is the number of rows in the matrix
    string2_len =len(t)+1

    #create a dictionary to store the matrix values
    edit_distance_matrix = {}

    for i in range(string1_len): edit_distance_matrix[i,0]=i
    for j in range(string2_len): edit_distance_matrix[0,j]=j
    #print(edit_distance_matrix)

    for i in range(1, string1_len):
        #print()
        #print("processing column:", i)

        for j in range(1, string2_len):
            #print("processing row:",j)

            insertion = edit_distance_matrix[i, j-1]+1
            deletion = edit_distance_matrix[i-1, j]+1
            match = edit_distance_matrix[i-1, j-1]
            mismatch = edit_distance_matrix[i-1, j-1]+1

            if(s[i-1] == t[j-1]):
                edit_distance_matrix[i,j] = min(insertion,deletion,match)
            else:
                edit_distance_matrix[i,j] = min(insertion,deletion,mismatch)

            #print(edit_distance_matrix)

    return edit_distance_matrix[i,j]

if __name__ == "__main__":
    print(edit_distance(input(), input()))
